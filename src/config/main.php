<?php

return [

    'facebook' => [
        'verify_token'      => 'verify_token',
        'page_access_token' => 'page_access_token',
        'text_character_limit' => 319,
        'failed_message'    => 'Sorry, but I am not able to comprehend what you are saying! I am just a bot, you see! Try asking me that in another way. :)',
        'unknown_message'    => 'Not sure about that one. I am still learning... :P',
        'maintainance_message'  =>  "Hey, thanks for your message. We are not here right now, but we'll get back to you soon!",
        'abusive_message'  =>  "That is so rude . Why would you say something like that? 😔",
        'generic_quick_reply_text'  => [
            "So, what's next?",
            "What do you want me to do next?",
            "How about I suggest something you can do?"
        ],
        'generic_quick_replies' => [
            [
                "content_type" => "text",
                "title" => "About Skarma",
                "payload" => "about_skarma"
            ],
            [
                "content_type" => "text",
                "title" => "Contact Skarma",
                "payload" => 'about_contact'
            ],
            [
                "content_type" => "text",
                "title" => "About Projects",
                "payload" => "about_projects"
            ],
            [
                "content_type" => "text",
                "title" => "Join Us",
                "payload" => "join_skarma"
            ],
            [
                "content_type" => "text",
                "title" => "Who are you?",
                "payload" => "smalltalk_about_bot"
            ],
            [
                "content_type" => "text",
                "title" => "(y)",
                "payload" => collect(['action' => 'user_feedback', 'feedback' => 'Good'])->toJson(),
                "image_url" => "https://scontent.xx.fbcdn.net/t39.1997-6/851557_369239266556155_759568595_n.png?_nc_ad=z-m"
            ],
            [
                "content_type" => "text",
                "title" => "👎",
                "payload" => collect(['action' => 'user_feedback', 'feedback' => 'Bad'])->toJson(),
                "image_url" => "https://image.freepik.com/free-icon/thumbs-down-silhouette_318-41911.png"
            ]
        ]
    ],

    'witai' => [
        'version' => '20160928',
        'base_url' => 'https://api.wit.ai/',
        'server_access_token' => 'server_access_token',
        'client_access_token' =>'client_access_token',
        'session_token' => 'session_token'
    ],

    'profanity_filters' => [
        'en-us'
    ],

    'data_file_url' => base_path().'\vendor\skarma-tech\botframework\src\Http\DataController.php'
];
