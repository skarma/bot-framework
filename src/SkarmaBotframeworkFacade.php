<?php

namespace SkarmaTech\Botframework;

use Illuminate\Support\Facades\Facade;

class SkarmaBotframeworkFacade extends Facade
{
    protected static function getFacadeAccessor() { 
        return 'skarma-tech-botframework';
    }
}
