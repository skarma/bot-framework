<?php
namespace SkarmaTech\Botframework;

use Illuminate\Support\ServiceProvider;

class SkarmaBotframeworkServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('skarma-tech-botframework');

        $this->mergeConfigFrom(
            __DIR__ . '/config/main.php', 'botframework'
        );
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/config/main.php' => config_path('botframework.php')
        ], 'config');
        
        require __DIR__ . '/Http/routes.php';
    }
}