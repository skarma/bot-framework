<?php

namespace SkarmaTech\Botframework\Http;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use SkarmaTech\Botframework\Http\Requests;

use SkarmaTech\Botframework\Http\FacebookRequestController;
use SkarmaTech\Botframework\Http\FacebookResponseController;
use SkarmaTech\Botframework\Http\FacebookUserController;
use SkarmaTech\Botframework\Http\WitaiResponseController;

use SkarmaTech\Botframework\models\EnvironmentProperty;
use SkarmaTech\Botframework\models\FacebookRequest;
use SkarmaTech\Botframework\models\FacebookUser;
use SkarmaTech\Botframework\models\FacebookResponse;

use Illuminate\Support\Facades\Input;
use Config;
use Log;

require_once(config('botframework.data_file_url'));

class WebhookController extends Controller
{
    public function index()
    {   
        $inputs = Input::all();
        
        if(empty($inputs['hub_verify_token'])){
            echo "You are not supposed to be here, only facebook allowed";
            return;
        }
        
        $verify_token = $inputs['hub_verify_token'];

        if(!is_null($verify_token) && $verify_token === config('botframework.facebook.verify_token')) {
            echo $inputs['hub_challenge'];
        }
    }

    public function store(Request $request)
    {   
        FacebookUser::init();
        FacebookRequest::init();
        FacebookResponse::init();
        $facebook_responses = collect();
        
        Log::info('========= RawFacebookRequest - START =============');
        Log::info($request);
        Log::info('========= RawFacebookRequest - END =============');

        FacebookUserController::storeFacebookUsers($request);
        
        // Save all facebook requests
        $facebook_requests = FacebookRequestController::storeFacebookRequests($request);

        /*
            Process individual facebook requests to determine what the response should be
        */
        foreach($facebook_requests as $facebook_request) {

            // Send typing response status
            $facebook_request->acknowledgeRequestReceipt();

            if(EnvironmentProperty::inMaintainance()) {  /* Are we Under Maintainance */

                $facebook_response = new FacebookResponse([
                    'facebook_request_id'   => $facebook_request->id,
                    'text'                  => config('botframework.facebook.maintainance_message')
                ]);

                $facebook_responses->push($facebook_response);

            }else if($facebook_request->isabusiveRequest()) {   /* Make sure the message is safe to answer */

                $facebook_response = new FacebookResponse([
                    'facebook_request_id'   => $facebook_request->id,
                    'text'                  => config('botframework.facebook.abusive_message')
                ]);

                $facebook_responses->push($facebook_response);

            }else if($facebook_request->isGreetingRequest()) {  /* Check if it's a new page visitor request */

                // [TODO] NOTICE : Switch to normal greeting after the campaign is over.
                // $facebook_response = new FacebookResponse([
                //     'facebook_request_id'   => $facebook_request->id,
                //     'collections'           => config('botframework.facebook.greeting_elements')
                // ]);
                // $facebook_responses->push($facebook_response);

                $facebook_response = new FacebookResponse([
                    'facebook_request_id'   => $facebook_request->id,
                    'text'                  => "Whats up buddy! I'm Skima. How's it going?. So I'll be your one stop guide about everything happening in Skarma. News, Projects, About Us, Contact Info.. You name it."
                ]);
                $facebook_responses->push($facebook_response);

                $facebook_response = new FacebookResponse([
                    'facebook_request_id'   => $facebook_request->id,
                    'quick_replies' => [
                        'text' => config('botframework.facebook.generic_quick_reply_text')[array_rand(config('botframework.facebook.generic_quick_reply_text'))],
                        'replies' => config('botframework.facebook.generic_quick_replies')
                    ]
                ]);
                $facebook_responses->push($facebook_response);

            } else if($facebook_request->isPostbackRequest()) { /* Custom Request, needs to be handled without NLP */

                Log::info('========= POSTBACK REQUEST =============');

                $data = DataController::queryData($facebook_request->payload, $facebook_request);
                foreach(FacebookResponse::processData($data, $facebook_request, false) as $facebook_response) {
                    $facebook_responses->push($facebook_response);
                }

                if(!$facebook_response->hasQuickReplies()) {
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'quick_replies' => [
                            'text' => config('botframework.facebook.generic_quick_reply_text')[array_rand(config('botframework.facebook.generic_quick_reply_text'))],
                            'replies' => config('botframework.facebook.generic_quick_replies')
                        ]
                    ]);
                    $facebook_responses->push($facebook_response);
                }

            } else if($facebook_request->isAttachmentRequest()) {

                /* Reply with this message for attachments i.e images, audio, video, etc */

                if($facebook_request->attachment_url == 'https://scontent.xx.fbcdn.net/t39.1997-6/851557_369239266556155_759568595_n.png?_nc_ad=z-m') {
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'text'                  => '(y)'
                    ]);
                }else{
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'text'                  => "Comeon, you really want me to understand that!! I am bot, remember?"
                    ]);
                }

                $facebook_responses->push($facebook_response);

                if(!$facebook_response->hasQuickReplies()) {
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'quick_replies' => [
                            'text' => config('botframework.facebook.generic_quick_reply_text')[array_rand(config('botframework.facebook.generic_quick_reply_text'))],
                            'replies' => config('botframework.facebook.generic_quick_replies')
                        ]
                    ]);
                    $facebook_responses->push($facebook_response);
                }
                
            }else {    /* Let's ask NLP to help us find a solution to this query message */

                // NLP + ML on facebook request  
                $raw_witai_response = $facebook_request->queryTextWithWitai();
                $witai_response = WitaiResponseController::storeWitaiResponse($raw_witai_response, $facebook_request);

                if($witai_response->hasAction()) {

                    // Thank you NLP for helping us understand that request, we will take it from here.
                    // If action is defined with custom namespace, that signifies we need to pass control to DataController

                    Log::info('========= Custom action namespace =============');
                    $data = DataController::queryData($witai_response, $facebook_request);
                    foreach(FacebookResponse::processData($data, $facebook_request, false) as $facebook_response) {
                        $facebook_responses->push($facebook_response);
                    } 

                }else {    

                    // We don't have any response available, let's apologize 
                    Log::info('========= No match =============');
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'text'                  => config('botframework.facebook.failed_message')
                    ]);
                    $facebook_responses->push($facebook_response);
                }

                if(!$facebook_response->hasQuickReplies()) {
                    $facebook_response = new FacebookResponse([
                        'facebook_request_id'   => $facebook_request->id,
                        'quick_replies' => [
                            'text' => config('botframework.facebook.generic_quick_reply_text')[array_rand(config('botframework.facebook.generic_quick_reply_text'))],
                            'replies' => config('botframework.facebook.generic_quick_replies')
                        ]
                    ]);
                    $facebook_responses->push($facebook_response);
                }
            }

            // Remove typing response status
            $facebook_request->acknowledgeRequestCompletion();
        }

        /*
            Process responses gathered
        */
        foreach($facebook_responses as $facebook_response) {
            $facebook_response->sendFacebookMessage();
        }
        
        // DynamoDbController::logToDb($facebook_responses);
        FacebookResponseController::storeFacebookResponses($facebook_responses);
        FacebookResponseController::checkIfUnanswered($facebook_responses);
    }
}
