<?php 

namespace SkarmaTech\Botframework\Http;

use Config;
use Log;
use DB;

class DataController {
	public static function queryData($query_data, $facebook_request, $payload = null) {
		if(!empty($query_data->action) && !empty($query_data->parameters)){
            $action = $query_data->action;
            $parameters = json_decode($query_data->parameters);
        }else if(is_string($query_data) && !empty($query_data)) {
            $parameters = json_decode($query_data);
            if( ! empty($parameters->action))
                $action =  $parameters->action;
            else
                $action = $query_data;
        } 

        $data = [
            'texts' => [
                Config::get('services.facebook.unknown_message')
            ]
        ];

        switch($action) {

            case 'smalltalk_greetings': {
                $data = [
                    'texts'  => [
                        "Hey, what's up? :D"
                    ]
                ];
                break;
            }

            case 'smalltalk_praise': {
                $data = [
                    'texts'  => [
                        "Glad I could help! :)"
                    ]
                ];
                break;
            }

            case 'smalltalk_about_bot': {
                $data = [
                    'texts'  => [
                        "I'm Skima, Skarma's Private Assistant! 3:)"
                    ]
                ];
                break;
            }

            case 'smalltalk_acknowledge': {
                $data = [
                    'texts'  => [
                        "Ok..."
                    ]
                ];
                break;
            }

            case 'smalltalk_forgive': {
                $data = [
                    'texts'  => [
                        "It's Ok! :)"
                    ]
                ];
                break;
            }

            case 'smalltalk_apologize': {
                $data = [
                    'texts'  => [
                        "I'm really sorry. Everybody makes mistakes, right? :P"
                    ]
                ];
                break;
            }

            case 'smalltalk_measurebot': {
                $data = [
                    'texts'  => [
                        "Wisdom is not measured by age",
                        "Intelligence is not measured by grades",
                        "Personality is not measured by what others say"
                    ]
                ];
                break;
            }

            case 'about_projects': {
                $data = [
                    'texts'  => [
                        "I'm really sorry. Everybody makes mistakes, right? :P"
                    ]
                ];
                break;
            }

            case 'about_contact': {
                $data = [
                    'texts' => [
                        'Mail Us at info@skarma.com',
                        'Reach Us at 022 4022 7117'
                    ],
                    'collections' => [
                        [
                            'title'     => 'Skarma, Mumbai',
                            'image_url' => 'https://s3.amazonaws.com/skarmabot/Maps.jpg',
                            'subtitle'  => '31, Evergreen Industrial Estate, Shakti Mills Lane, Mumbai, India 400011',
                            'buttons' => [
                                [
                                    'type'  => 'web_url',
                                    'url'   => 'https://goo.gl/maps/gRVHwWLBh962',
                                    'title' => 'Show on Map'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'https://mail.google.com/mail/?view=cm&fs=1&to=info@skarma.com&su=INQUERY%20MAIL%20FROM%20MESSENGER&body='.htmlspecialchars('YOUR MESSAGE HERE'),
                                    'title' => 'Write to Us (Gmail)'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'http://skarma.com/',
                                    'title' => 'Visit Website'
                                ]
                            ]
                        ],
                        [
                            'title'     => 'Skarma, Goa',
                            'image_url' => 'https://s3.amazonaws.com/skarmabot/Maps-Goa.png',
                            'subtitle'  => 'FC Goa, FC Goa House, H.No.850, Oﬀ N.H.17, Porvorim North, Goa GA 403251',
                            'buttons' => [
                                [
                                    'type'  => 'web_url',
                                    'url'   => 'https://goo.gl/maps/iJJYeo59kaQ2',
                                    'title' => 'Show on Map'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'https://mail.google.com/mail/?view=cm&fs=1&to=info@skarma.com&su=INQUERY%20MAIL%20FROM%20MESSENGER&body='.htmlspecialchars('YOUR MESSAGE HERE'),
                                    'title' => 'Write to Us (Gmail)'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'http://skarma.com/',
                                    'title' => 'Visit Website'
                                ]
                            ]
                        ]
                    ]
                ];
                break;
            }

            case 'about_skarma': {
                $data = [
                    'texts' => [
                        'We’re proud to be an agency in the cloud. A technology platform that’s collaborating with creative professionals globally, to build meaningful communities around big ideas.'
                    ],
                    'collections' => [
                        [
                            'title'     => 'Skarma',
                            'image_url' => 'https://s3.amazonaws.com/skarmabot/skarma-card.jpg',
                            'subtitle'  => 'Building brands around communities and communities around brands.',
                            'buttons' => [
                                [
                                    'type' => 'web_url',
                                    'url' => 'https://jobspire.net/companies/skarma',
                                    'title' => 'Join Us'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'http://skarma.com/',
                                    'title' => 'Visit Website'
                                ]
                            ]
                        ]
                    ]
                ];
                break;
            }

            case 'join_skarma': {
                $data = [
                    'texts' => [
                        'We’re proud to be an agency in the cloud. A technology platform that’s collaborating with creative professionals globally, to build meaningful communities around big ideas.',
                        'You can mail us your resume at jobs@skarma.com'
                    ],
                    'collections' => [
                        [
                            'title'     => 'Skarma Jobs',
                            'image_url' => 'http://skarma.com/images/we-are-looking-for.jpg',
                            'subtitle'  => 'Building brands around communities and communities around brands.',
                            'buttons' => [
                                [
                                    'type' => 'web_url',
                                    'url' => 'https://mail.google.com/mail/?view=cm&fs=1&to=jobs@skarma.com&su=Job%20Application%20for%20&body='.htmlspecialchars('please attach your resume with a cover letter in this mail!'),
                                    'title' => 'Apply Now (Gmail)'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'https://jobspire.net/companies/skarma',
                                    'title' => 'Via Jobspire'
                                ],
                                [
                                    'type' => 'web_url',
                                    'url' => 'http://skarma.com/',
                                    'title' => 'Visit Website'
                                ]
                            ]
                        ]
                    ]
                ];
                break;
            }

            case 'about_socialmedia': {
                if(!empty($parameters->social_link[0])){
                    switch(strtolower($parameters->social_link[0]->value)){
                        case 'facebook':
                        case 'fb':
                            $data = [
                                'texts'  => [
                                    "https://www.facebook.com/skarmaonline/"
                                ]
                            ];
                        break;

                        case 'twitter':
                            $data = [
                                'texts'  => [
                                    "https://twitter.com/skarmaonline"
                                ]
                            ];
                        break;

                        case 'youtube':
                            $data = [
                                'texts'  => [
                                    "https://www.youtube.com/user/skarmaonline"
                                ]
                            ];
                        break;

                        case 'linkedin':
                            $data = [
                                'texts'  => [
                                    "https://www.linkedin.com/company/skarma"
                                ]
                            ];
                        break;

                        case 'google':
                            $data = [
                                'texts'  => [
                                    "https://plus.google.com/+skarmaonline"
                                ]
                            ];
                        break;

                        case 'instagram':
                            $data = [
                                'texts'  => [
                                    "https://www.instagram.com/skarmaonline/"
                                ]
                            ];
                        break;

                        case 'default':
                            $data = [
                                'texts'  => [
                                    "Sorry, we are not on {$parameters->social_link}"
                                ]
                            ];
                    }
                }
                break;
            }

            case 'user_feedback': {
                if($parameters->feedback == 'Good'){
                    $data = [
                        'texts'  => [
                            "I am glad to know that. Will keep rocking it!! 8-)"
                        ]
                    ];
                }else {
                    $data = [
                        'texts'  => [
                            "I am sorry about that, well I am still learning. 3:)"
                        ]
                    ];
                }
                break;
            }

            default : {
                $data = [
                    'texts'  => [
                        Config::get('services.facebook.unknown_message')
                    ]
                ];
            }

        }
        return $data; 
    }
}