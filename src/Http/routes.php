<?php

Route::group(array('prefix' => 'v1'), function() {
    Route::resource('webhook', '\SkarmaTech\Botframework\Http\WebhookController', ['only' => ['index', 'store']]);
});