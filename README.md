# Demo Laravel Package

## Copy whole project under packages/skarma/botframework

## Add below code to your main composer.json file

"autoload": {
    "psr-4": {
        "App\\": "app/",
        "Skarma\\Botframework\\": "packages/skarma/botframework/src"
    }
}

## Run composer install inside "packages/skarma/botframework"